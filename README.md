#VSRG Austria Discord Bot

A tiny bot that does stuff, yay.
Can save and shoutout quotes, manages the quotes' sources, and also calculates some server stats. Gets updated regularly, per demand of the VSRG Austria Discord server.

To run this as a bot, an authentication token from the [Discord Developer Portal](https://discordapp.com/developers/applications/) is needed in form of an /auth.json file, as well as an API Key that enables querying public Google Calendars.
It's in the .gitignore for obvious reasons.

```JS
{
	"token": "YOUR_TOKEN",
	"calKey": "YOUR_GOOGLE_API_KEY"
}
```

Data will be generated automatically if there is nothing there. If you want to feed it with stuff /data/userquotes.json and /data/stats.json are the target files, though stats probably shouldn't be touched by hand, as it is just a bunch of Discord ids and tags.

In theory you should be able to download this repo, run npm install, and be able to run it by `node src/main.js`. Some npm dependencies may be deprecated (because of course they are, the npm culture is broken), I might fix this at some point, but no promises.