const userQuotes = require('./quotes.json');
let newDb = { users: {}, quotes: {} };

let quoteId = 0;
let userId = 0;
Object
  .entries(userQuotes)
  .forEach(([ user, quotes ]) => {
    newDb.users[userId] = { name: user, quotes: [] };

    quotes.forEach(q => { 
      newDb.quotes[quoteId] = q;
      newDb.users[userId].quotes.push(quoteId);

      quoteId++;
    });

    userId++;
  });

const fs = require('fs');
fs.writeFile('db.json', JSON.stringify(newDb, null, 2), (err) => {
  if (err) throw err;
  console.log('File created successfully');
});

// console.log(newDb);