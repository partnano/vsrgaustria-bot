const https = require('https');
const calKey = require('../auth.json').calKey;

function sendCurrentEvents(channel, withHeader, withNoFound)
{
    if (!channel)
    {
        console.error("No channel for sendCurrentEvents");
        return;
    }

    if (!channel.type === "text")
    {
        console.error("No text channel for sendCurrentEvents");
        return;
    }

    if (!channel.send)
    {
        console.error("No send function for channel");
        return;
    }
    
    retrieveCurrentEvents((events, birthdays) => {
        if (!events)
        {
            if (withNoFound) channel.send("Nothing's happening today.\n\nFor details, see:\n<https://calendar.google.com/calendar/embed?src=prefix.aut@gmail.com>");
            return;
        }

        let msg = withHeader
            ? ">>> **Good morning, something's happening!**\n\n"
            : ">>> ";

        msg += events;
        msg += '\nFor details, see:\n<https://calendar.google.com/calendar/embed?src=prefix.aut@gmail.com>'

        channel.send(msg);

        if (!withHeader) return;
        for (birthdayBoy of birthdays)
        {
            channel.send(`<@${birthdayBoy}> Happy Birthday!`);

            let guild = channel.client.guilds.first();
            let member = guild.members.find(m => m.user.id == birthdayBoy);

            if (member)
                member.addRole("650257307061452801");
        }
    });
}

function retrieveCurrentEvents(callback)
{
    if (calKey === undefined) {
return;
    }
    let date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);

    let todayStr = date.getDate() + '';
    let todayMonthStr = (date.getMonth() + 1) + '';

    if (todayMonthStr.length === 1) todayMonthStr = "0" + todayMonthStr;
    
    // temp
    // date.setDate(date.getDate() - 1);

    let endDate = new Date(date);
    endDate.setDate(date.getDate() + 2);

    const options = {
        // hostname: `www.googleapis.com/calendar/v3/calendars/prefix.aut%40gmail.com/events?key=${calKey}`,
        hostname: `content.googleapis.com`,
        path: `/calendar/v3/calendars/prefix.aut%40gmail.com/events?key=${calKey}&timeMax=${endDate.toISOString()}&timeMin=${date.toISOString()}`,
        method: 'GET',
        port: 443
    };

    const req = https.request(options, res => {
        let data = "";

        res.on('data', d => {
            data += d;
        });

        res.on('end', () => {
            data = JSON.parse(data);
            let items = data.items.filter(item => item.status !== 'cancelled');
            let birthdays = [];
            
            if (items.length == 0)
            {
                callback(null);
                return;
            }
            
            items.sort((a, b) => {
                const aDate = a.start && a.start.date;
                const bDate = b.start && b.start.date;

                if (aDate && bDate)
                {
                    if (aDate === bDate) return 0;
                    else if (aDate < bDate) return -1;
                    else /* aDate > bDate */ return 1;
                }
                else
                {
                    return 0;
                }
            });

            let msg = "";
            items.forEach(item => {
                if (item.start && item.start.date)
                {
                    const month = item.start.date.substring(5, 7);
                    const day = item.start.date.substring(8, 10);

                    if (todayStr == day && todayMonthStr == month)
                    {
                        msg += '`Today:` ';

                        if (item.summary.startsWith("GB:"))
                            birthdays.push(item.location);
                    }
                    else
                        msg += '`' + `${day}.${month}` + ':` ';
                }

                msg += item.summary;
                msg += '\n';
            });

            callback(msg, birthdays);
        });
    });

    req.on('error', error => {
        console.error(error);
    });

    req.end();
}

module.exports =
    {
        sendCurrentEvents
    };
