const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const { prefix } = require('../../config.json');
//const Discord = require('discord.js');
const { findFuzzyUser, getQuotes, getUsers, getDB } = require('../db-helpers.js');

module.exports = {
	name : 'alphazitat',
	args: false, //['firstLetters'], // pucgenie: causes problems with complex user input blocking etc.
	usage: '<firstLetters> [<numberOfWords> [:<userN>:]]',
	aliases: ['miniquote', 'outismus', 'pseudoquote',],
	examples: [
			`${prefix}outismus helloworld`,
			`${prefix}miniquote musik 0 prefix`,
			`${prefix}alphazitat memen 30 azrael fortisest`,
		],
	description: "Generates a pseudo quote out of the words with letters beginning with a letter for each in <firstLetter> of all (or one user's) collected quotes. ...Yippie. <user> may be * for all. <numberOfWords> may be 0 to derive from <firstLetters> character count.",

	execute(message, args,) {
		"use strict";
		let [firstLetters, numberOfWords = "0", ...users] = args;
		// pucgenie: I don't know how to cleanly write that:
		if ((users?.length ?? 1) === 0) {
			// undefined is a singleton, and this block is to ensure equivalent behaviour between v8 and node.js later on.
			users = undefined
		}

		if (typeof(firstLetters) !== 'string' || firstLetters.length < 1) {
			message.channel.send('Hmm ... these arguments look strange!');
	return;
		}
		firstLetters = firstLetters.toLowerCase()

		numberOfWords = (numberOfWords === "0")
			? firstLetters.length
			: parseInt(numberOfWords, 10,)
		;

		if (numberOfWords < 1 || numberOfWords > 200) {
			message.channel.send('Sorry, only between 1 and 200 words :(');
	return;
		}
		
		// pucgenie: premature optimization
		const quotesByFirstLetter = {
			a: [],
			b: [],
			c: [],
			d: [],
			e: [],
			f: [],
			g: [],
			h: [],
			i: [],
			j: [],
			k: [],
			l: [],
			m: [],
			n: [],
			o: [],
			p: [],
			q: [],
			r: [],
			s: [],
			t: [],
			u: [],
			v: [],
			w: [],
			x: [],
			y: [],
			z: [],
		};
		{
			// pucgenie: premature optimization
			// 'cause we need two things from userquotes.json
			let db = getDB()
			if (users !== undefined) {
				const cachedUsers = getUsers(db)
				users = users.map(user => findFuzzyUser(user, cachedUsers,));
			}
			//console.log("alphazitat after caching users")

			{
				// get quotes from database filtered by selected users
				let quotesList;
				{
					const quotesMap = getQuotes(db);
					// pucgenie: premature optimization, gc
					db = null
					
					// filter by user if needed
					quotesList =
						users?.flatMap(user => user.quotes.map(quote => quotesMap[quote]))
						?? Object.values(quotesMap)
					;
					//console.log("alphazitat after preparing quotesList:", quotesList, "from quotesMap:", quotesMap, "users:", users,)
					// gc
				}

				// get all words from all quotes and put (as arrays) into a map keyed by their first letters
				for (const word of new Set(quotesList.flatMap(fullQuoteText => fullQuoteText.split(/\s+/g)))) {
					(quotesByFirstLetter[word[0].toLowerCase()] ??= []).push(word);
				}
				//console.log("alphazitat after populating quotesByFirstLetter")

				// gc
			}
			// gc, forget database
		}

		// pucgenie: How about the readability; better or insanely worse?
		const lettersToLookFor = new Array(numberOfWords)
		
		//console.log("alphazitat before preparing lettersToLookFor")
//		{
//			// pucgenie: experimental optimization
//			lettersToLookFor.length = 0
//			//let debugI = 0
//			do {
//				Array.prototype.push.apply(lettersToLookFor, Array.from(firstLetters),)
//			//	//console.log(debugI++, lettersToLookFor.length, numberOfWords, firstLetters,)
//			} while (lettersToLookFor.length < numberOfWords); // pucgenie: Almost caused an infinite loop -.-
//			if (lettersToLookFor.length > numberOfWords) {
//				lettersToLookFor.length = numberOfWords
//			}
//		}
		for (let i = 0; i < numberOfWords; ++i) {
			lettersToLookFor[i] = firstLetters[i % firstLetters.length]
		}
		//console.log("alphazitat after preparing lettersToLookFor")

		// pucgenie: premature optimization, gc
		firstLetters = null
		
		// pucgenie: premature optimization
		const out = new Array(numberOfWords + 2);
		// pucgenie: could have introduced `const numberOfWords_fr = numberOfWords` and re-used/re-purposed numberOfWords as i - but it would have been very confusing.
		let i = 0;
		out[i++] = '>';
		out[numberOfWords + 1] = `- **${users?.map(uo => uo.name).join(', ') ?? "VSRG Austria"}**`;

		// pucgenie: premature optimization, gc
		users = null;

		//console.log("alphazitat before working loop")
		//console.log(lettersToLookFor, quotesByFirstLetter);
		// TODO: pop() used letters if it's possible without much hassle and if it really reduces resource usage^^. That would improve debugging I think.
		for (const letter of lettersToLookFor) {
			const wordsBucket = quotesByFirstLetter[letter]
			if (wordsBucket === undefined) {
				// special symbol/letter that didn't occur in quotes yet
		continue;
			}
			const theRWord = wordsBucket[Math.floor(Math.random() * wordsBucket.length)]
			if (theRWord !== undefined) {
				out[i++] = theRWord
			}
		}
		if (i < numberOfWords) {
			out[i++] = out[numberOfWords + 1];
			out.length = i;
		}

		message.channel.send(out.join(' '));
	}
}
