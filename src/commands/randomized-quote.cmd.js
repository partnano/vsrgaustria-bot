const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const { prefix } = require('../../config.json');
const Discord = require('discord.js');
const { findFuzzyUser } = require('../db-helpers.js');

module.exports = {
    name : 'randomquote',
    args: false,
    usage: '<numberOfWords> | <user> | <user> <numberOfWords>',
    aliases: ['superquote', 'skynet', 'superzitat', 'autismus'],
    examples:
        [   `${prefix}superzitat`,
            `${prefix}autismus 3`,
            `${prefix}randomquote prefix`,
            `${prefix}skynet azrael 23`
        ],
    description: 'Generates a random quote out of the words of all collected quotes. ...Yeah.',

    execute(message, args) {
        let wordCount = 0;
        let user;

        // parse arguments and assign above variables
        if (args[0] && args[1])
        {
            if (isNaN(args[1]) || typeof(args[0]) !== 'string')
            {
                message.channel.send('Hmm ... I can\'t do anything with those arguments');
                return;
            }

            user = findFuzzyUser(args[0]);
            wordCount = args[1];
        }
        else if (args[0])
        {
            if (isNaN(args[0]) && typeof(args[0]) == 'string')
            {
                user = findFuzzyUser(args[0]);
                wordCount = Math.floor(Math.random() * 30) + 1;
            }
            else if (!isNaN(args[0]))
            {
                user = null;
                wordCount = args[0];
            }
            else
            {
                message.channel.send('Hmm ... these arguments look strange!');
                return;
            }
        }
        else
        {
            user = null;
            wordCount = Math.floor(Math.random() * 30) + 1;
        }

        if (wordCount < 1 || wordCount > 200)
        {
            message.channel.send('Sorry, only between 1 and 200 words :(');
            return;
        }

        // get all quotes
        const adapter = new FileSync('data/userquotes.json');
        const db = low(adapter);
        let quotesMap = db.get('quotes').value();
        let quotesList = [];

        // filter by user if needed
        if (user)
        {
            user.quotes.forEach(quote => {
                quotesList.push(quotesMap[quote]);
            });
        }
        else quotesList = Object.values(quotesMap);

        // generate list of words
        const quotesWords = quotesList
                            .reduce((all, curr) => all + ' ' + curr, '')
                            .split(' ');

        // generate random quote
        let out = "";
        for (; wordCount > 0; --wordCount)
        {
            out += quotesWords[Math.floor(Math.random() * quotesWords.length)] + " ";
        }

        // send it
        out = out.trim();
        outUser = (user && user.name) || "VSRG Austria"
        message.channel.send(`> ${out} - **${outUser}**`);
    }
}