module.exports = {
  name: 'emotes',
  args: false,
  usage: '',
  aliases: ['emojis', 'emoticons'],
	description: 'Prints all server emotes.',
  
  execute(message) {
    const emotes = message.guild.emojis.map(e => e);
    
    let chunked = []
    for (let i = 0; i < emotes.length; i += 50)
        chunked.push(emotes.slice(i, i + 50))

    chunked.forEach(
      chunk => message.channel.send(
        chunk.reduce(
          (out, e) => out + e, ''
        )
      )
    );

  }
}