const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const { prefix } = require('../../config.json');
const path = require('path');
const crypto = require('crypto');
const appDir = path.dirname(require.main.filename);
const Fuse = require('fuse.js');
const fs = require('fs');
const { getQuotes, getUsers, getDB, findUser, findFuzzyUser } = require('../db-helpers.js');

module.exports = {
  name: 'quote',
  args: false,
  usage: '<user> | sources <|string> | all <user> | add|remove <user|[user1, user2, ...]> "<quote>" | download <format>',
  examples: 
    [ `${prefix}quote AzraeL`
    , `${prefix}quote sources azr`
    , `${prefix}quote all partnano`
    , `${prefix}quote add SirChronus "irgendwas schlaues"`
    , `${prefix}quote remove PreFiXAUT "irgendeine pun"`
    , `${prefix}quote add [PreFiXAUT, SirChronus] "irgendein dev gospel"`
    ],
  aliases: ['zitat', 'anonüm',],
	description: 'Prints and manages quotes. Prints either a user quote, all user quotes, or just a random quote. I can give you all the source names, or some matching names. If you tell me user and quote, I can add it to or remove it from the repository!',
  
  execute(message, args, commandName) {
    switch(args[0]) {
      case 'all': 
        execAllUserQuotes(message, args);
        break;

      case 'sources':
        execSources(message, args);
        break;

      case 'add': 
        execAdd(message, args);
        break;

      case 'remove': 
        execRemove(message, args);
        break;

      case 'download':
        execDownload(message, args);
        break;

      case undefined:
        execBase(message, commandName != 'anonüm');
        break;

      default: 
        execUserQuote(message, args, commandName != 'anonüm');
    }
  }
}

// ALL USER QUOTES

function execAllUserQuotes (message, args) {
  args.shift();
  const user = args.join(' ');

  if (user.length === 0) {
    execDownload(message);
    return
  }

  // message.channel.send(
  //   findAllQuotesByUser(user)
  // );

  findAllQuotesByUser(user).forEach(chunk => {
    message.channel.send(chunk);
  });
}

function findAllQuotesByUser (username) {
  let user = findUser(username);

  if (!user)
    user = findFuzzyUser(username);

  if (!user)
    return [`Can't find a user by name \'${username}\', sorry! :(`];

  const quotes = getQuotes();
  let chunk = '';
  let output = [`All quotes by **${user.name}**:`];
  
  user.quotes.forEach(quote => {
    const extQuote = `> ${quotes[quote]}\n`;

    if (chunk.length + extQuote.length >= 1900) {
      output.push('```' + chunk + '```');
      chunk = '';
    }

    chunk += extQuote;
  });

  output.push('```' + chunk + '```');

  // // discord message splitting for maxchars
  // for(let i = 0; i < all.length; i += 1990) {
  //   const part = all.substring(i, Math.min(all.length, i + 1990));

  //   output.push('```' + part + '```');
  // }

  return output;
}

// LIST SOURCES

function sort (arr) {
  let newArr = [...arr];
  newArr.sort(
    (first, second) => 
      first.toLowerCase() > second.toLowerCase() ? 1 : -1
  );
  return newArr;
}

function findFuzzySources (username) {
  const fuse = new Fuse(getUsers(), {keys: ['name'], threshold: 0.3});
  const result = fuse.search(username);

  return result.map(entry => entry.item.name);
}

function execSources (message, args) {
  args.shift();
  const users = getUsers();
  const mc = message.channel;
  const user = args.join(' ');

  if (user)
    mc.send(findFuzzySources(user));
  else 
    mc.send(sort(users.map(u => u.name)));
}

// ADD QUOTES

function execAdd (message, args) {
  args.shift();
  const mc = message.channel;

  const [usernames, quote] = validateInput(message, args);
  if (!usernames || !quote) return;

  const users = usernames.map(u => findOrCreateUser(u, mc));

  if (users.length > 0) {
    const userSaid = users.find(user => findQuote(user, quote));

    if (userSaid) {
      mc.send(`${userSaid.name} already said that, so I can't add it. Sorry!`);
      purgeUsers();
      return;
    }

    const id = createQuote(quote);
    users.forEach(user =>
      getDB()
        .get('users')
        .find({name: user.name})
        .get('quotes')
        .push(id)
        .write()
    );
  }

  let usersprint = users.reduce((out, user) => 
    out + `**${user.name}**, `, '');

  usersprint = usersprint.substring(0, usersprint.length -2);
  usersprint += '!';

  mc.send(`> ${quote}\nwas added to source${users.length === 1 ? '' : 's'} ${usersprint}`);
}

function createQuote(quote) {
  const id = createId(quote);
  getDB().get('quotes').assign({[id]: quote}).write();

  return id;
}

function findOrCreateUser(username, mc) {
  let user = findUser(username);
  if (user) return user;

  const fuzzyUser = findFuzzyUser(username);
  if (fuzzyUser)
    mc.send(`Careful! I can't find **${username}**, but maybe you meant **${fuzzyUser.name}**?`);

  user = {name: username, quotes: []};
  getDB()
    .get('users')
    .push(user)
    .write();

  return user;
}

// REMOVE QUOTES

function execRemove (message, args) {
  args.shift();
  const mc = message.channel;

  const [usernames, quote] = validateInput(message, args);
  if (!usernames || !quote) return;

  const users = usernames.map(u => findUser(u)).filter(e => e);

  if (users.length === 0) {
    const fuzzyUsers = usernames.map(u => findFuzzyUser(u)).filter(e => e);
    let userstr = fuzzyUsers.reduce((out, u) => out += `**${u.name}**, `, '');
    userstr = userstr.substring(0, userstr.length - 2) + '?';

    if (userstr)
      mc.send("Can't find any of those, did you mean " + userstr);
    else
      mc.send("Can't find anyone. You sure they exist?");
  
    return;
  }

  if (users.length !== usernames.length) {
    usernames.forEach(u => {
      if (!findUser(u)) {
        const fuzzyUser = findFuzzyUser(u);
        if (fuzzyUser)
          mc.send(`Couldn't find **${u}**, did you mean **${fuzzyUser.name}**?`);
        else
          mc.send(`Couldn't find **${u}**, sorry!`);
      }
    });
  }

  const qid = users.reduce((qid, user) => qid = findQuote(user,quote) || qid, undefined);

  if (!qid) {
    mc.send("Can't find this quote. Maybe it's from a different user?");
    return;
  }

  users.forEach(user =>
    getDB()
      .get('users')
      .find({ name: user.name })
      .get('quotes')
      .remove(q => q === qid)
      .write()
  );

  tryRemoveQuote(qid);
  purgeUsers();

  const userstr = users.reduce((out,u) => out + `**${u.name}**, `,'');
  mc.send(`Quote's gone for ${userstr.substring(0, userstr.length - 2)}!`);
}

function tryRemoveQuote(quoteid) {
  const user =
    Object
      .values(getUsers())
      .find(u => u.quotes.includes(quoteid));

  if (!user) {
    getDB().get('quotes').unset(quoteid).write();
  }
}

// BASE - NO ARGS

function execBase (message, printUsersNames) {
  message.channel.send(findRandomQuote(printUsersNames));
}

function findRandomQuote (printUsersNames) {
  const quotes = Object.keys(getQuotes());
  const randQuote =
    quotes[quotes.length * Math.random() << 0];

  return printQuote(randQuote, printUsersNames);
}

// RANDOM USER QUOTE

function execUserQuote (message, args, printUsersNames) {
  const user = args.join(' ');

  message.channel.send(
    findRandomQuoteByUser(user, printUsersNames)
  );
}

function findRandomQuoteByUser (username, printUsersNames) {
  let user = findUser(username);

  if (!user) {
    user = findFuzzyUser(username);
  }

  if (!user)
    return `There is no source by name \`${username}\`, sorry! :(`;
  
  const quote = user.quotes[user.quotes.length * Math.random() << 0];
  return printQuote(quote, printUsersNames);
}

// DOWNLOAD QUOTES.JSON
function execDownload (message, args) {
  const format = args ? args[args.length -1] : 'txt';

  const quotes = getQuotes();
  const users = getUsers();

  let content;
  let extension;

  switch (format) {
    case 'json':
      extension = 'json';

      let out = {};
      users.forEach(u => out[u.name] = u.quotes.map(q => quotes[q]));
      content = JSON.stringify(out);
      
      break;

    default:
      extension = 'txt';
      content = 
        Object
          .values(quotes)
          .reduce((out, quote) => out += `"${quote}",\n`, '');
  }

  const filepath = appDir + "/../data/quotes_download." + extension;
  fs.writeFile(filepath, content, err => {
    if (err) throw err;
  });

  message.channel.send({
    files: [filepath]
  });
}

// HELPERS

function purgeUsers() {
  const users = getUsers();
  users.forEach(
    (u) => {
      if (u.quotes.length === 0)
        getDB().get('users').remove({name: u.name}).write();
    }
  );
}

function createId (str) {
  const sha1 = crypto.createHash('sha1');
  sha1.update(str);
  return sha1.digest('hex');
}

function findQuote (user, quote) {
  const quotes = getQuotes();
  return user.quotes.find(qid => quotes[qid] === quote);
}

function printQuote (quote, printUsersNames) {
  const quotes = getQuotes();
  if (printUsersNames) {
    const users = getUsers()
      .filter(u => u.quotes.includes(quote));
    let userstr = users.reduce((out, u) => out + `**${u.name}**, `, '');
    userstr = userstr.substring(0, userstr.length -2);
    return `>>> ${quotes[quote]} - ${userstr}`;
  } else {
    return `>>> ${quotes[quote]}`;
  }
}

function validateInput (message, args) {
  if (!message.member.hasPermission("MANAGE_GUILD")) {
    message.channel.send("Only Admins & Mods can do that!");
    return [];
  }

  const [userstr, quote] = 
    args.map(str => str.trim());

  if (!userstr || !quote)
    return printWrongArgsMsg(message.channel);

  let users = [userstr];

  if (userstr.startsWith('[') 
    && userstr.endsWith(']') 
    && userstr.includes(',')) {
      users = userstr
        .substring(1,userstr.length -1)
        .split(',')
        .map(u => u.trim());
  }

  return [users, quote];
}

function printWrongArgsMsg (channel) {
  channel.send("The arguments look strange! Try `user \"quote\"`.");
  return [];
}