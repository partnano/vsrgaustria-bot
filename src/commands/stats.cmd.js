const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const Discord = require('discord.js');

module.exports = {
  name: 'stats',
  args: true,
  usage: 'quotes|messages|emotes',
	description: 'Calculates some statistics.',
	execute(message, args) {
    switch(args[0]) {
      case 'quotes':
        execQuotes(message);
        break;
      
      case 'messages':
        execMessages(message);
        break;
      
      case 'emotes':
        execEmotes(message);
        break;

      default:
        message.channel
          .send(`The argument ${args[0]} doesn't seem to exist!`);
    }
	},
};

// MESSAGES

async function execMessages (message) {
  const [channels, merged] = await fetchData(message);

  let userMessageCount = {}

  Object
    .entries(merged.users)
    .forEach(([key, user]) => userMessageCount[key] = user.messages);

  let out = new Discord.RichEmbed()
    .setColor('#212121')
    .setTitle(`${message.guild.name} Message Statistics`)
    .setThumbnail(message.guild.iconURL)
    .setDescription(`${merged.messages} messages over all channels, ${merged.allMessages} including bots. This is how much you spam, just saying.`)
    .addField('Top Users', getTopMessages(userMessageCount, 10, '@'))
    .addField('Top Channels', getTopMessages(merged.channels, 5, '#'))
    .setTimestamp();

  message.channel.send(out);
}

// data = { key: num, ...}
function getTopMessages (data, size, prefix, fill = 'messages') {
  const tuples = Object.entries(data).map(arr => arr);
  tuples.sort(([_1, v1], [_2, v2]) => v2 - v1);
  
  const top = tuples.slice(0, size);
  return top.reduce(
    (out, [id, value]) => out + `<${prefix}${id}> - \`${value}\` ${fill}\n`,
    ""
  );
}

async function execEmotes (message) {
  const [channels, merged] = await fetchData(message);
  
  let sumEmotes = 0;
  let perUser = {};
  Object
    .entries(merged.users)
    .forEach(([userid, user]) => {
      Object
        .values(user.emotes)
        .forEach(count => {
          sumEmotes += count;
          
          if (perUser[userid]) perUser[userid] += count;
          else perUser[userid] = count;
        });
    });

  let perEmote = {};
  let perChannel = {};
  channels.forEach(channel => {
    perChannel[channel.id] = 0;

    Object
      .values(channel.users)
      .forEach(user => { 
        perEmote = mergeEmoteStats(perEmote, user.emotes);
        
        Object
          .values(user.emotes)
          .forEach(count => perChannel[channel.id] += count);
      })
  });

  let out = new Discord.RichEmbed()
    .setColor('#212121')
    .setTitle(`${message.guild.name} Emoji Statistics`)
    .setThumbnail(message.guild.iconURL)
    .setDescription(`${sumEmotes} emotes over all channels.`)
    .addField('Most Used', getTopEmotes(perEmote, 10, message.client))
    .addField('Top Users', getTopMessages(perUser, 5, '@', 'emotes'))
    .addField('Top Channels', getTopMessages(perChannel, 5, '#', 'emotes'))
    .setTimestamp();

  message.channel.send(out);
}

// data = { key: num, ...}
function getTopEmotes (data, size, client) {
  const tuples = Object.entries(data).map(arr => arr);
  tuples.sort(([_1, v1], [_2, v2]) => v2 - v1);
  
  const top = tuples.slice(0, size);
  return top.reduce(
    (out, [id, value]) => {
      const name = id.slice(1, -1);
      let emote = client.emojis.find(emoji => emoji.name === name);

      if (!emote) emote = id;

      return out + `${emote} - \`${value}\` times\n`
    }, ""
  );
}

async function fetchData(message) {
  const textChannels = 
    message.guild.channels
      .filter(c => c.type === 'text');

  const adapter = new FileSync('data/stats.json');
  const db = low(adapter);

  const promises = 
    textChannels.map(c => processChannel(c, db));

  message.channel
    .send("This might take a while!" +
      " I'll report back as soon as I have everything ...");

  const channels = await Promise.all(promises);
  const merged = channels.reduce(
    mergeChannels,
    { allMessages: 0, messages: 0, users: {}, channels: {} }
  );

  return [channels, merged];
}

async function processChannel (channel, db) {
  const pre = db.get(channel.id).value();
  const newest = pre ? pre.newest : undefined;

  let before;
  let stats =
    pre ||
    { id: channel.id
    , name: channel.name
    , users: {}
    , allMessages: 0
    , messages: 0
    };
  
  // that is gonna be new no matter what
  stats.newest = undefined;

  let end = false;
  while (true) {
    const messages =
      await channel.fetchMessages({ limit: 100, before: before });

    for (let i = 0; i < messages.array().length; ++i) {
      const msg = messages.array()[i];

      if (newest && msg.id === newest) {
        end = true;
        break;
      }

      stats.allMessages++;

      if (msg.author.bot) continue;

      const id = msg.author.id;

      if (stats.users[id]) {
        stats.users[id].messages++;
        stats.users[id].emotes = mergeEmoteStats(
          stats.users[id].emotes, 
          extractEmotes(msg.content)
        );
      }
      else {
        stats.users[id] =
          { name: msg.author.username
          , messages: 1
          , emotes: extractEmotes(msg.content)
          }
      }

      stats.messages++;
    }

    if (!stats.newest) stats.newest = messages.firstKey();

    before = messages.lastKey();

    if(messages.size < 100 || end) break;
  }

  db.set(channel.id, stats).write();
  return stats;
}

// returns new stats
function mergeEmoteStats (prev, emotes) {
  let newStats = Object.assign({}, prev);

  Object
    .entries(emotes)
    .forEach(([emote, count]) => {
      if (newStats[emote]) newStats[emote] += count;
      else newStats[emote] = count;
    });

  return newStats;
}

function extractEmotes (content) {
  const emoteRegex = /:[A-Za-z0-9]+:/g;
  const emotes = content.matchAll(emoteRegex);

  let stats = {};
  for (const emote of emotes) { 
    if (stats[emote]) ++stats[emote];
    else stats[emote] = 1;
  };

  return stats;
}

function mergeChannels (prev, channel) {
  let users = Object.assign({}, prev.users);

  for (let [key, user] of Object.entries(channel.users)) {
    if (users[key]) {
      users[key].messages += user.messages;
      users[key].emotes = mergeEmoteStats(users[key].emotes, user.emotes);
    }
    else {
      users[key] =
        { messages: user.messages
        , emotes: user.emotes
        }
    }
  }

  let channels = Object.assign({}, prev.channels, {[channel.id]: channel.messages});

  return {
    allMessages: prev.allMessages + channel.allMessages,
    messages: prev.messages + channel.messages,
    users,
    channels
  };
}

// QUOTES

function execQuotes (message) {
  const adapter = new FileSync('data/userquotes.json');
  const db = low(adapter);
  const users = db.get('users').value();

  const amountSources = users.length;
  const amountQuotes =
    users.reduce(
      (sum, user) => sum + user.quotes.length
    , 0);
  
  const top5 = 
    printQuoteStats(getTopMostQuotes(users, 5));

  let out = new Discord.RichEmbed()
    .setColor('#212121')
    .setTitle(`${message.guild.name} Quote Statistics`)
    .setThumbnail('https://cdn.discordapp.com/attachments/289869872446570496/633683872021872660/idjqoijqi.png')
    .setDescription(`\`${amountSources}\` different sources said \`${amountQuotes}\` different questionable things.`)
    .addField('Top Sources', top5)
    .setTimestamp();

  message.channel.send(out);
}

function getTopMostQuotes (all, size) {
  let sorted = 
    Object
      .values(all)
      .map(user => user);
    
  sorted.sort(
    (firstUser, secondUser) =>
      secondUser.quotes.length - firstUser.quotes.length 
  );

  return sorted.slice(0, size);
}

function printQuoteStats (quoteTuples) {
  const stats = quoteTuples
    .reduce(
      (out, user) =>
        out += `**${user.name}** with \`${user.quotes.length}\` quotes.\n`
    , '');

  return stats;
}
