const { sendCurrentEvents } = require('../calendar.js');

module.exports = {
    name: 'events',
    args: false,
    description: 'Prints today\'s events, according to the VSRG Austria google calendar.',

    execute(message, args)
    {
	sendCurrentEvents(message.channel, false, true);
    }
}
