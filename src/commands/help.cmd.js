const { prefix } = require('../../config.json');

module.exports = {
	name: 'help',
	description: 'List all of my commands or info about a specific command.',
	aliases: ['commands'],
	usage: '[command name]',
  
	execute(message, args) {
		const data = [];
    const { commands } = message.client;

    if (args.length) {
      const name = args[0].toLowerCase();
      const command = 
        commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

      if (!command) {
        return message.reply('Don\'t know that one, sorry!');
      }

      data.push(`**Name:** ${command.name}`);

      if (command.aliases)
        data.push(`**Aliases:** ${command.aliases.join(', ')}`);
      if (command.description)
        data.push(`**Description:** ${command.description}`);
      if (command.usage)
        data.push(`**Usage:** \`${prefix}${command.name} ${command.usage}\``);
      if (command.examples) {
        data.push('**Examples:**');
        command.examples.forEach(example =>
          data.push('`' + example + '`'));
      }

    }
    else { 
      data.push(`Everything I know, right here (use one with \`${prefix}help <cmd>\`):`);
      data.push(
        '```' +
        commands.map(command => command.name).join(', ') +
        '```'
      );
      data.push('For all the info, you can find my inner workings right here:');
      data.push('<https://bitbucket.org/partnano/vsrgaustria-bot>');
      data.push('Feel free to suggest any changes to @partnano!');
    }
    
    message.channel.send(data, { split: true });
	},
};