const Fuse = require('fuse.js');
const low = require('lowdb');
//import { LowSync } from 'lowdb'

const FileSync = require('lowdb/adapters/FileSync');
//import { JSONSyncFile } from 'lowdb/node'
//const lowdbnode = require('lowdb/node')

function getQuotes(db)
{
  return (db??getDB()).get('quotes').value();
}

function getUsers(db)
{
  return (db??getDB()).get('users').value();
}

function getDB()
{
  const adapter = new FileSync('data/userquotes.json');
  const db = low(adapter);
  //return new low.LowSync(new lowdbnode.JSONFileSync('data/userquotes.json', 
  db.defaults({quotes: {}, users: [],}).write();
  return db;
}

function findUser(username, cachedUsers, db,)
{
  return (cachedUsers??getUsers(db)).find(u => u.name === username);
}

function findFuzzyUser(username, cachedUsers, db,)
{
  const fuse = new Fuse(cachedUsers ?? getUsers(db), {keys: ['name'], threshold: 0.3});
  const result = fuse.search(username);

  if (result.length > 0)
    return result[0].item;
}

module.exports =
{
  getQuotes,
  getUsers,
  getDB,
  findUser,
  findFuzzyUser
};
