const fs = require('fs');
const Discord = require('discord.js');
const config = require('../config.json');

// not in git repo, needs to be provided
const token = require('../auth.json').token;

const client = new Discord.Client();
client.commands = new Discord.Collection();
client.reactions = new Discord.Collection();

const commandFiles = 
      fs.readdirSync('./src/commands')
      .filter(file => file.endsWith('.cmd.js'));

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
}

const prefix = config.prefix;

client.on('message', message => {

    if (!message.content.startsWith(prefix) || message.author.bot)
        return;

    const args = splitArgs(message);
    const commandName = args.shift().toLowerCase();
    const command = client.commands.get(commandName)
          || client.commands.find(
              cmd => cmd.aliases && cmd.aliases.includes(commandName)
          );

    if (!command) return;

    if (command.args && !args.length) {
        let reply = "Try something in the form of ";
        reply += `\`${prefix}${command.name} ${command.usage}\``;

        return message.channel.send(reply);
    }
    
    try {
        command.execute(message, args, commandName);
    }
    catch (error) {
        console.error(error.message);
    }

});

// can take string in "-marks as last argument
// ex. $bla blub "foo bar" -> ['bla', 'blub', 'foo bar']
function splitArgs (message) {
    let out = [];
    let content = message.content;
    content.trim();

    const quoteIndex = content.indexOf("\"");
    if (quoteIndex === -1)
        return content.slice(prefix.length).split(/ +/);

    const bracketIndex = content.indexOf("[");
    if (bracketIndex === -1)
        out = content.slice(prefix.length, quoteIndex -1).split(/ +/);
    else {
        out = content.slice(prefix.length, bracketIndex -1).split(/ +/);
        out.push(content.slice(bracketIndex, quoteIndex -1));
    }
    
    const quote = content.slice(quoteIndex +1, content.length -1);
    if (quote) out.push(quote);
    return out;
}

client.once('ready', () => {
    console.log('Ready!');

    // -- [ CALENDAR STUFF ] --

    const { sendCurrentEvents } = require('./calendar.js');

    const schedule = require('node-schedule');
    const job = schedule.scheduleJob('00 00 07 * * 0-6',
                                     () => sendCurrentEvents(client.channels.find(el => el.name == "events"), true));
});

client.on('error', (err) => {
    console.error(err.message)
});

client.login(token);
