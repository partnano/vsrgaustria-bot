const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

// my discord community is strange ...
// why again am I doing so much for them?
module.exports = {
  name: 'owo',

  execute(message, owo) {
    switch(owo) {
      case "owo":
        answerAndCount(message, 'What\'s this?');
        break;
      case "OwO":
        answerAndCount(message, 'Notices your buwulge OwO');
        break;
      case "uwu":
        answerAndCount(message, 'Wis is so sad');
        break;
      case "owo?":
        message.channel.send(
          `You owo'd ${getDB().get('owoCount').value() || 0} times`
        );
        break;
      default:
        console.warn(`Invalid owo argument: ${owo}`);
    }
  }
}

// HELPERS

function answerAndCount(message, answer) {
  message.channel.send(answer);
  const db = getDB();
  const count = db.get('owoCount').value() || 0;
  
  db
    .set('owoCount', count + 1)
    .write();
}

function getDB () {
  const adapter = new FileSync('data/misc.json');
  return low(adapter);
}